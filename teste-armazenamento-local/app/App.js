import React,{useState} from 'react';
import { StyleSheet, Text, View,Button,AsyncStorage} from 'react-native';
import Usuario from './model/Usuario'
import Treino from  './model/Treino'

/*var treino1 = new Treino("agachamento",10,3,[1,3,5,4],"agachar","seu peso")
var treino2 = new Treino("felxão",10,3,[2,0,6],"aopio de frente",60)
treino1.concluir
var vetor = []
vetor.push(treino1)
vetor.push(treino2)
var user = new Usuario("carlos",22,"101010","26/11/1999","Rua jandira, 1100","85985319265","emagrecimento","quatro vezes por semana","Carlos Jorge",vetor);*/


async function loading(){
  let user = new Usuario("","","","","","","","","","","")
  let res = await user.carregar()
  alert(JASON.stringify(res))
  res = null
  return res
}

export default function App() {

  

  let user = loading()
  const [usuario,setUser] = useState(user,()=>{})

  if(user == null){
    return(
      <View>
        <Text>Paia</Text>
      </View>
    )
  }else{
    return (
      <View style={styles.container}>
        <Text>{JSON.stringify(usuario)}</Text>
        <Button title='clique aqui' onPress={()=>{user.getTreinosHoje()}}/>
        <Button title="Add treino" onPress={()=>{user.addTreino("agachamento",10,3,[1,3,5,4],"agachar","seu peso");setUser(user)}}/>
        <Button title="Deletar treino" onPress={()=>{user.excluirTreino("agachamento")}}/>
        <Button title="Salvar" onPress={async ()=>{user.salvar()}}/>
        <Button title="Carregar" onPress={()=>{user.carregar()} } />
      </View>
    );
  }
  
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});



