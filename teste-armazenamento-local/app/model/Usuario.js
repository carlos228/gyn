import React from 'react'
import {AsyncStorage} from 'react-native'
import treino from './Treino'
import Treino from './Treino'
import verificaExercicio from '../utils/VerificaElementeEmVetor'

export default class Usuario {

    static inicioNulo(){
        this.imagem = null
        this.nome = null
        this.email = null
        this.senha = null
        this.nascimento = null
        this.enderesso = null
        this.contato = null
        this.objetivo = null
        this.frequencia = null
        this.orientador = null
        this.treinos = []
        this.treinos.concat(null)
        this.dadaInicio = null
        this.data = null
        this.peso = null
        this.altura = null
        this.coxaDireita = null
        this.coxaEsquerda = null
        this.quadril = null
        this.partDir = null
        this.partEsq = null
        this.abdomem = null
        this.cintura = null
        this.torax = null
        this.bracoDireito = null
        this.bracoEsquerdo = null
        this.antebracoEsquerdo = null
        this.antebracoDireito = null
        this.punhoDireito = null
        this.punhoEsquerdo = null

        return this
    }

    constructor(nome,email,senha,nascimento,enderesso,contato,objetivo,frequencia,orientador,treinos,dataRegistro){
        this.imagem = null
        this.nome = nome
        this.email = email
        this.senha = senha
        this.nascimento = nascimento
        this.enderesso = enderesso
        this.contato = contato
        this.objetivo = objetivo
        this.frequencia = frequencia
        this.orientador = orientador
        this.treinos = []
        this.treinos.concat(treinos)
        this.dadaInicio = dataRegistro
        this.data = ""
        this.peso = 0
        this.altura = 0
        this.coxaDireita = 0
        this.coxaEsquerda = 0
        this.quadril = 0
        this.partDir = 0
        this.partEsq = 0
        this.abdomem = 0
        this.cintura = 0
        this.torax = 0
        this.bracoDireito = 0
        this.bracoEsquerdo = 0
        this.antebracoEsquerdo = 0
        this.antebracoDireito = 0
        this.punhoDireito = 0
        this.punhoEsquerdo = 0
    }


    getTreinosHoje(){
        var  treinosHoje = []
        const tam = this.treinos.length
        var diaHoje = new Date()
        for(let treino = 0;treino < tam ;treino++){
            this.treinos[treino].validaHoje()
        }
        for(let i = 0;i<tam;i++){
            if(this.treinos[i].hoje && !this.treinos[i].concluido){
                treinosHoje.push(this.treinos[i])
                this.treinos[i].concluir()
            }else{
                //treinosHoje.push(this.treinos[i])
            }
        }
        alert(JSON.stringify(treinosHoje))

    }

    addTreino(nome,qtdRepeticoes,qtdSeries,diasDaSerie,descricao,carga){
        let serie = new Treino(nome,qtdRepeticoes,qtdSeries,diasDaSerie,descricao,carga);
        if(!verificaExercicio(nome,this.treinos)){
            this.treinos.push(serie)
        }
    }

    excluirTreino(nome){ 
        this.treinos.splice(this.treinos.indexOf({nome:nome}),1)
    }
    

    atualizarDados(peso,altura,coxaDireita,coxaEsquerda,quadril,partDir,partEsq,abdomem,cintura,torax,bracoDireito,bracoEsquerdo,antebracoDireito,antebracoEsquerdo,punhoDireito,punhoEsquerdo){
        this.data = 0 //Capturar a data de hoje
        this.peso = peso
        this.altura = altura
        this.coxaDireita = coxaDireita
        this.coxaEsquerda = coxaEsquerda
        this.quadril = quadril
        this.partDir = partDir
        this.partEsq = partEsq
        this.abdomem = abdomem
        this.cintura = cintura
        this.torax = torax
        this.bracoDireito = bracoDireito
        this.bracoEsquerdo = bracoEsquerdo
        this.antebracoEsquerdo = antebracoDireito
        this.antebracoDireito = antebracoEsquerdo
        this.punhoDireito = punhoDireito
        this.punhoEsquerdo = punhoEsquerdo
    }

    async salvar(){
        try {
            await AsyncStorage.setItem('user', JSON.stringify(this));
          } catch (error) {
        
          } 
    }

    async carregar(){
        try{
            var someText = await AsyncStorage.getItem('use')
            someText = JSON.parse(someText)
            this.imagem = someText.imagem
            this.nome = someText.nome
            this.email = someText.email
            this.senha = someText.senha
            this.nascimento = someText.nascimento
            this.enderesso = someText.enderesso
            this.contato = someText.contato
            this.objetivo = someText.objetivo
            this.frequencia = someText.frequencia
            this.orientador = someText.orientador
            this.treinos = []
            this.treinos.concat(someText.treinos)
            this.dadaInicio = someText.dataRegistro
            this.data = someText.data
            this.peso = someText.peso
            this.altura = someText.altura
            this.coxaDireita = someText.coxaDireita
            this.coxaEsquerda = someText.coxaEsquerda
            this.quadril = someText.quadril
            this.partDir = someText.partDir
            this.partEsq = someText.partEsq
            this.abdomem = someText.abdomem
            this.cintura = someText.cintura
            this.torax = someText.torax
            this.bracoDireito = someText.bracoDireito
            this.bracoEsquerdo = someText.bracoEsquerdo
            this.antebracoEsquerdo = someText.antebracoEsquerdo
            this.antebracoDireito = someText.antebracoDireito
            this.punhoDireito = someText.punhoDireito
            this.punhoEsquerdo = someText.punhoEsquerdo
            alert(JSON.stringify(someText)+"oi: "+someText.nome)
            return this
        }catch(error){
            alert("Erro")
            this = null
        }
    }



}