import React from 'react'
import {} from 'react-native'
import vetorEmVetor from '../utils/VetorEmVetor'

export default class Treino{
    constructor(nome,qtdRepeticoes,qtdSeries,diasDaSerie,descricao,carga){
        this.nome = nome
        this.quantidaDeRepeticoes = qtdRepeticoes
        this.quantidaDeSeries = qtdSeries
        this.diasDaSerie = diasDaSerie
        this.descricao = descricao
        this.carga = carga
        this.concluido = false
        this.hoje = false
    }

     concluir() {
        this.concluido = true
    }

    validaHoje(){
        var diaHoje = new Date()
        let hojeDia = diaHoje.getDay()
        let vetor = [0,0,3,4]
        if(vetorEmVetor([hojeDia],this.diasDaSerie)){
            this.hoje = true
            //alert("1")
        }
        else{
            this.hoje = false
            //alert(hojeDia)
        }
        
    
    }

}