export default function vetorEmVetor(vetor1,vetor2){
    let a = []
    let tam1 = vetor1.length
    let tam2 = vetor2.length

    for(let i = 0;i<tam1;i++){
        for(let j = 0;j<tam2;j++){
            if(vetor1[i] == vetor2[j]){
                return true
            }
        }
    }

    return false
}